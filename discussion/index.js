 // 3 types of looping construct
/*
	1. While Loop
	2. Do-While Loop
	3. For Loop
*/

/*
	While Loop
	-Syntax
		while(expression/consition) {
		statement/s;
		}
*/

let count = 5;
while(count !== 0){
	console.log("While:" + count);
	count--; //count=count -1, count =-1;
	//decrement
}

/*
	Expected Output
	While 5
	While 4
	While 3
	While 2
	While 1
*/
let countNew = 1;

while(countNew <= 5){
	console.log("While:" + countNew);
	countNew++; //count=count +1, count =+1
	//decrement
}

/*Do while
	Syntax"
	do{
	statement;
	}while(expression/condition)	
	}

*/

let num1 =1;

do{
	console.log("Do-while:" + num1 );
	num1++;
}while(num1 < 6);
// convert prompt to number
// equilvalent to parseint-->Number
console.log("Number in console")
let num2 = Number(prompt("Give me a number:"));

do{
	console.log("Do-while: " + num2);

	num2 += 1;
}while(num2 <10);

/*
	For Loop
	Syntax:
	for(initialization; expression/condition; finalExpression){
	statement
	}
*/

for(let num3 = 1; num3 <= 5; num3++){
	console.log(num3)
}

console.log("For loop with prompt");

let newNum = Number(prompt("Enter a number: "));

for (let num4 = newNum; num4 < 10; num4++){
	console.log(num4);
}

// Display code depending on user's input
let counter = Number(prompt("Enter a number: "));

for (let num5 = 1;num5 !=(counter+1); num5++){
	console.log(num5);
}

/* Other solution:
for (let num5 = 1;num5 <=(count2); num5++){
	console.log(num5);
}
*/
console.log("String Iteration");

let myString ="Veronica";
console.log(myString.length);
// To access certain letter
console.log(myString[4]);
console.log(myString[7]);

console.log("Display individual letter")
// x- will represent the array
// myStringlenght== 7
for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

console.log("Display vowels 3")
// convert to uppercase

let myName = "VEronicA";
for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u' 
		) 
	{
		console.log(3);
	}
		else{
			console.log(myName[i]);
	}
}

console.log("Continue and Break")

/*
	Continue and Break Statements
	-continue statement allows the code to go to the next iteration
	of the loop w/o finishing the execution of all statements
	in a code block
	Break statement is used to terminate the current loop once a match
	has been found.
*/

for(let count = 0; count <=20; count++){
	if (count % 2 == 0){
		continue;
 }
 	console.log("Continue and Break: " + count);

 	if(count > 10) {
 		break;
 	}
}


