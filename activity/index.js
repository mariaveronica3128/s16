let count = Number(prompt("Enter a number: "));
console.log(count);

for (let i = count; i >= 0; i--){
	
	if(i <= 50){
		console.log("Loop has been terminated.");
		break;
	}
// < > <= >=, ==, ===, !=, !==
	else if (i % 10 == 0){
		console.log("This number is being skipped.");
		continue;
	}

	else if (i % 5 == 0){ 
		console.log(i);
		continue;
	}
	console.log(i);
}


let  myString ="supercalifragilisticexpialidocious";
let  myCons = "";
let  myVows = "";

for (let i = 0; i < myString.length; i++) {
	if (
		myString[i].toLowerCase() == 'a' ||
		myString[i].toLowerCase() == 'e' ||
		myString[i].toLowerCase() == 'i' ||
		myString[i].toLowerCase() == 'o' ||
		myString[i].toLowerCase() == 'u' 
		) 
	{
		myVows = myVows + myString[i];
	}

	else{
		myCons = myCons + myString[i];
	}
}

	console.log(myVows);
	console.log(myCons);
